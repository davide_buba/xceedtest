package com.github.bubinimara.xceed.http;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.github.bubinimara.xceed.model.Player;
import com.github.bubinimara.xceed.model.Team;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static com.github.bubinimara.xceed.test.R.raw.teams;
import static com.github.bubinimara.xceed.test.R.raw.players;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by davide on 14/10/16.
 *
 * !to swap assert parameters !
 */

public class HttpRequestTest {

    Context context;
    MockWebServer server = new MockWebServer();
    private String teamUrl = "/v1/soccerseasons/399/teams";
    private String playerUrl = "/v1/teams/560/players";


    @Before
    public void setUp() throws Exception {
        context = InstrumentationRegistry.getContext();
        server.start();
        server.setDispatcher(dispatcher);

    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

    final Dispatcher dispatcher = new Dispatcher() {

        @Override
        public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
            assertThat(request.getHeader("X-Auth-Token"),is("f7f2051260a8417b8eae9fb4de617af3"));

            try {
                if (request.getPath().equals(playerUrl)){
                    return new MockResponse()
                            .setBody(IOUtils.toString(context.getResources().openRawResource(players)))
                            .setResponseCode(200);

                }else {

                    return new MockResponse()
                            .setBody(IOUtils.toString(context.getResources().openRawResource(teams)))
                            .setResponseCode(200);
                }
            } catch (IOException e) {
                return new MockResponse().setResponseCode(404);
            }
        }

    };

    /**
     * See team.json file in raw folder
     * @throws Exception
     */
    @Test
    public void testTeams() throws Exception {
        ArrayList<Team> response = HttpRequest.getTeams(server.url(teamUrl).toString());
        assertNotNull(response);
        assertThat(20,is(response.size()));

        // test first
        assertThat("RC Deportivo La Coruna",is(response.get(0).getName()));
        assertThat("LAC",is(response.get(0).getCode()));
        assertThat("Deportivo",is(response.get(0).getShortName()));
        assertThat("67,000,000 €",is(response.get(0).getSquadMarketValue()));
        assertThat("http://upload.wikimedia.org/wikipedia/en/4/4e/RC_Deportivo_La_Coruña_logo.svg",
                is(response.get(0).getCrestUrl()));
        assertThat("http://api.football-data.org/v1/teams/560/players",is(response.get(0).getPlayersUrl()));

        // test second ..
        assertThat("Real Sociedad de Fútbol",is(response.get(1).getName()));
        assertThat("RSS",is(response.get(1).getCode()));
        assertThat("Real Sociedad",is(response.get(1).getShortName()));
        assertThat("111,300,000 €",is(response.get(1).getSquadMarketValue()));
        assertThat("http://upload.wikimedia.org/wikipedia/de/5/55/Real_Sociedad_San_Sebastián.svg",
                is(response.get(1).getCrestUrl()));
        assertThat("http://api.football-data.org/v1/teams/92/players",is(response.get(1).getPlayersUrl()));

    }

    @Test
    public void testPlayers() throws Exception {
        ArrayList<Player> response = HttpRequest.getPlayers(server.url(playerUrl).toString());
        assertNotNull(response);
        assertThat(response.size(),is(20));

        // test first
        assertThat((response.get(0).getName()),is("Guilherme"));
        assertThat((response.get(0).getPosition()),is("Defensive Midfield"));
        assertThat((response.get(0).getJerseyNumber()),is(20));
        assertThat((response.get(0).getDateOfBirth()),is("1991-04-05"));
        assertThat((response.get(0).getNationality()),is("Brazil"));
        assertThat((response.get(0).getContractUntil()),is("2017-06-30"));
        assertThat((response.get(0).getMarketValue()),is("2,000,000 €"));


        // test second
        assertThat("Rubén",is(response.get(1).getName()));
        assertThat("Keeper",is(response.get(1).getPosition()));
        assertThat(25,is(response.get(1).getJerseyNumber()));
        assertThat("1984-06-22",is(response.get(1).getDateOfBirth()));
        assertThat("Spain",is(response.get(1).getNationality()));
        assertThat("2018-06-30",is(response.get(1).getContractUntil()));
        assertThat("1,000,000 €",is(response.get(1).getMarketValue()));

    }
}
