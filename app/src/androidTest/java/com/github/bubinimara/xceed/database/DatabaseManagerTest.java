package com.github.bubinimara.xceed.database;

import android.support.test.InstrumentationRegistry;

import com.github.bubinimara.xceed.model.Player;
import com.github.bubinimara.xceed.model.Team;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by davide on 14/10/16.
 */

public class DatabaseManagerTest {
    private DatabaseManager databaseManager;

    @Before
    public void setUp() throws Exception {
        databaseManager = DatabaseManager.getInstance(InstrumentationRegistry.getTargetContext());
    }

    @After
    public void tearDown() throws Exception {
        databaseManager.deleteAll();
    }

    @Test
    public void testTeams() throws Exception {
        ArrayList<Team> teams = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            teams.add(new Team("name "+i,"code "+i,"s name "+i,"value "+i,"cest "+i,"player "+i));
        }

        databaseManager.getTeams();
        assertThat(databaseManager.getTeams().size(),is(0));

        databaseManager.addTeams(teams);
        ArrayList<Team> query = databaseManager.getTeams();
        assertThat(query.size(),is(teams.size()));
        for (int i = 0; i < 10; i++) {
            assertThat(query.get(i).getName(),is(teams.get(i).getName()));
            assertThat(query.get(i).getCode(),is(teams.get(i).getCode()));
            assertThat(query.get(i).getShortName(),is(teams.get(i).getShortName()));
            assertThat(query.get(i).getSquadMarketValue(),is(teams.get(i).getSquadMarketValue()));
            assertThat(query.get(i).getCrestUrl(),is(teams.get(i).getCrestUrl()));
            assertThat(query.get(i).getPlayersUrl(),is(teams.get(i).getPlayersUrl()));
        }

        databaseManager.deleteTeams();
        assertThat(databaseManager.getTeams().size(),is(0));

    }


    @Test
    public void testPlayers() throws Exception {
        String hashKey1 = "hashKey1";

        ArrayList<Player> players = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            players.add(new Player("name "+i,"position ",i,"birthday "+i,"nationali "+i,"contract "+i,"market "+i));
        }

        databaseManager.getPlayers(hashKey1);
        assertThat(databaseManager.getPlayers(hashKey1).size(),is(0));

        databaseManager.addPlayers(hashKey1,players);
        ArrayList<Player> query = databaseManager.getPlayers(hashKey1);
        assertThat(query.size(),is(players.size()));
        for (int i = 0; i < 10; i++) {
            assertThat(query.get(i).getName(),is(players.get(i).getName()));
            assertThat(query.get(i).getPosition(),is(players.get(i).getPosition()));
            assertThat(query.get(i).getJerseyNumber(),is(players.get(i).getJerseyNumber()));
            assertThat(query.get(i).getDateOfBirth(),is(players.get(i).getDateOfBirth()));
            assertThat(query.get(i).getNationality(),is(players.get(i).getNationality()));
            assertThat(query.get(i).getContractUntil(),is(players.get(i).getContractUntil()));
            assertThat(query.get(i).getMarketValue(),is(players.get(i).getMarketValue()));
        }

        databaseManager.deletePlayer(hashKey1);
        assertThat(databaseManager.getPlayers(hashKey1).size(),is(0));

    }

}
