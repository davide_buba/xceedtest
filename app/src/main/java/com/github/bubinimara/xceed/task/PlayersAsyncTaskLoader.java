package com.github.bubinimara.xceed.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.github.bubinimara.xceed.Loader;
import com.github.bubinimara.xceed.model.Player;
import com.github.bubinimara.xceed.model.Team;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by davide on 14/10/16.
 */

public class PlayersAsyncTaskLoader extends AsyncTaskLoader<AsyncTaskResult<ArrayList<Player>>> {
    private AsyncTaskResult<ArrayList<Player>> data = new AsyncTaskResult<>();

    private Loader loader;
    private String url;
    private boolean isLoaded = false;


    public PlayersAsyncTaskLoader(Context context,String url) {
        super(context);
        loader = new Loader(context);
        this.url = url;
    }

    @Override
    public AsyncTaskResult<ArrayList<Player>> loadInBackground() {
        try {
            data.setData(loader.getPlayer(url));
        } catch (IOException e) {
            data.setException(e);
        }
        isLoaded = true;
        return data;
    }


    @Override
    public void deliverResult(AsyncTaskResult<ArrayList<Player>> data) {
        this.data= data;
        if(isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(isLoaded){
            deliverResult(data);
        }else{
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();

    }

}
