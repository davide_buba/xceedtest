package com.github.bubinimara.xceed.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by davide on 14/10/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "database";
    private static final int DB_VERSION = 1;


    private static final String CREATE_TABLE_TEAMS =
            "CREATE TABLE " + DatabaseContract.Teams.TABLE_NAME +
                    " ( " +
                    DatabaseContract.Teams._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    DatabaseContract.Teams.COLUMN_NAME + " TEXT NOT NULL, " +
                    DatabaseContract.Teams.COLUMN_CODE + " TEXT, " +
                    DatabaseContract.Teams.COLUMN_SHORT_NAME + " TEXT, " +
                    DatabaseContract.Teams.COLUMN_MARKET_VALUE + " TEXT, " +
                    DatabaseContract.Teams.COLUMN_CREST_URL + " TEXT NOT NULL, " +
                    DatabaseContract.Teams.COLUMN_PLAYER_URL + " TEXT NOT NULL, " +

                    DatabaseContract.Teams.COLUMN_UPDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP " +
                    " );";

    private static final String CREATE_TABLE_PLAYERS =
            "CREATE TABLE " + DatabaseContract.Player.TABLE_NAME +
                    " ( " +
                    DatabaseContract.Player._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    DatabaseContract.Player.COLUMN_HASH + " TEXT NOT NULL, " +
                    DatabaseContract.Player.COLUMN_NAME + " TEXT NOT NULL, " +
                    DatabaseContract.Player.COLUMN_POSITION + " TEXT, " +
                    DatabaseContract.Player.COLUMN_NUMBER + " INTEGER, " +
                    DatabaseContract.Player.COLUMN_BIRTHDAY + " TEXT, " +
                    DatabaseContract.Player.COLUMN_NATIONALITY + " TEXT, " +
                    DatabaseContract.Player.COLUMN_CONTRACT_UNTIL + " TEXT, " +
                    DatabaseContract.Player.COLUMN_MARKET_VALUE + " TEXT, " +

                    DatabaseContract.Teams.COLUMN_UPDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP " +
                    " );";


    private static final String DELETE_TABLE_TEAMS = "DROP TABLE IF EXISTS " + DatabaseContract.Teams.TABLE_NAME;
    private static final String DELETE_TABLE_PLAYERS = "DROP TABLE IF EXISTS " + DatabaseContract.Player.TABLE_NAME;

    private static DatabaseHelper instance;

    private DatabaseHelper(Context context) {
        this(context, null);
    }

    private DatabaseHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DB_NAME, factory, DB_VERSION);
    }

    /**
     * Get single instance of this class
     * @param context the application context
     * @return the instance of this class
     */
    static synchronized DatabaseHelper getInstance(Context context) {
        if(instance == null){
            instance = new DatabaseHelper(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TEAMS);
        db.execSQL(CREATE_TABLE_PLAYERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(DELETE_TABLE_PLAYERS);
        db.execSQL(DELETE_TABLE_TEAMS);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETE_TABLE_PLAYERS);
        db.execSQL(DELETE_TABLE_TEAMS);
        onCreate(db);

    }
}
