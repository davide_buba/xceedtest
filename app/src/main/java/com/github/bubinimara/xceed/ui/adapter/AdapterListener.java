package com.github.bubinimara.xceed.ui.adapter;

/**
 * Created by davide on 15/10/16.
 */

public interface AdapterListener {
    void onItemClick(int position);
}
