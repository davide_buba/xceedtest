package com.github.bubinimara.xceed;

import android.app.Application;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.github.bubinimara.xceed.ui.svg.SvgDecoder;
import com.github.bubinimara.xceed.ui.svg.SvgDrawableTranscoder;
import com.github.bubinimara.xceed.ui.svg.SvgSoftwareLayerSetter;

import java.io.InputStream;

import static com.github.bubinimara.xceed.R.id.imageView;
import static java.security.AccessController.getContext;

/**
 * Created by davide on 14/10/16.
 */

public class App extends Application {

    private static GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;

    @Override
    public void onCreate() {
        super.onCreate();

        requestBuilder = Glide.with(this)
                .using(Glide.buildStreamModelLoader(Uri.class, this), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .listener(new SvgSoftwareLayerSetter<Uri>());
    }

    public static GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> getRequestBuilder() {
        return requestBuilder;
    }
}
