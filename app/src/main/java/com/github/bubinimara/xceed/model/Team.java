package com.github.bubinimara.xceed.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by davide on 14/10/16.
 */

public class Team implements Parcelable {
    private String name;
    private String code;
    private String shortName;
    private String squadMarketValue;
    private String crestUrl;

    private String playersUrl;

    /**
     * Empty constructor
     */
    public Team() {
    }

    /**
     *
     * @param name Name of the team
     * @param code code
     * @param shortName short name
     * @param squadMarketValue value
     * @param crestUrl url of the image url
     * @param playersUrl link of the player api
     */
    public Team(String name, String code, String shortName, String squadMarketValue, String crestUrl, String playersUrl) {
        this.name = name;
        this.code = code;
        this.shortName = shortName;
        this.squadMarketValue = squadMarketValue;
        this.crestUrl = crestUrl;
        this.playersUrl = playersUrl;
    }

    protected Team(Parcel in) {
        name = in.readString();
        code = in.readString();
        shortName = in.readString();
        squadMarketValue = in.readString();
        crestUrl = in.readString();
        playersUrl = in.readString();
    }

    public static final Creator<Team> CREATOR = new Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel in) {
            return new Team(in);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSquadMarketValue() {
        return squadMarketValue;
    }

    public void setSquadMarketValue(String squadMarketValue) {
        this.squadMarketValue = squadMarketValue;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public void setCrestUrl(String crestUrl) {
        this.crestUrl = crestUrl;
    }

    public String getPlayersUrl() {
        return playersUrl;
    }

    public void setPlayersUrl(String playersUrl) {
        this.playersUrl = playersUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(code);
        dest.writeString(shortName);
        dest.writeString(squadMarketValue);
        dest.writeString(crestUrl);
        dest.writeString(playersUrl);
    }
}
