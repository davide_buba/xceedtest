package com.github.bubinimara.xceed.http;

import com.github.bubinimara.xceed.model.Player;
import com.github.bubinimara.xceed.model.Team;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;

/**
 * Created by davide on 14/10/16.
 *  Util class to create model from Json response
 */

public class JsonToModelConverter {

    /**
     * @param jsonResponse
     * @return
     * @throws JsonSyntaxException
     */
    public static final ArrayList<Team> createTeams(String jsonResponse) throws JsonSyntaxException{
        ArrayList<Team> result = new ArrayList<>();

        JsonObject jo = (new JsonParser()).parse(jsonResponse).getAsJsonObject();
        JsonArray teams = jo.getAsJsonArray("teams");

        for (int i = 0; i < teams.size(); i++) {
            result.add(createTeam(teams.get(i)));
        }

        return result;
    }

    /**
     * Workaround to inner object
     */
    private static Team createTeam(JsonElement jsonElement) throws JsonSyntaxException {
        Team team;
        team =  new Gson().fromJson(jsonElement,Team.class);
        team.setPlayersUrl(jsonElement.getAsJsonObject().get("_links")
                .getAsJsonObject().get("players").getAsJsonObject().get("href").getAsString());
        return team;
    }

    public static ArrayList<Player> createPlayers(String jsonResponse) throws JsonSyntaxException {
        ArrayList<Player> result = new ArrayList<>();
        Gson gson = new Gson();

        JsonObject jo = (new JsonParser()).parse(jsonResponse).getAsJsonObject();
        JsonArray players = jo.getAsJsonArray("players");

        for (int i = 0; i < players.size(); i++) {
            result.add(gson.fromJson(players.get(i),Player.class));
        }
        return result;
    }
}
