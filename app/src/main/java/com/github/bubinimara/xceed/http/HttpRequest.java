package com.github.bubinimara.xceed.http;

import com.github.bubinimara.xceed.model.Player;
import com.github.bubinimara.xceed.model.Team;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by davide on 14/10/16.
 *
 * Handle api calls
 */

public class HttpRequest {
    // http client - no cache because required as db -
    private static final OkHttpClient client = new OkHttpClient();

    /**
     * Get all teams
     * @return the teams as ArrayList
     * @throws IOException if an error occurs
     */
    public static ArrayList<Team> getTeams() throws IOException {
        return getTeams("http://api.football-data.org/v1/soccerseasons/399/teams");
    }

  /**
     * Get all teams
     *  @param url the api url
     * @return the teams as ArrayList
     * @throws IOException if an error occurs
     */
    public static ArrayList<Team> getTeams(String url) throws IOException {
        Request request = new Builder().url(url).build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String body = response.body().string();


        try {
            return JsonToModelConverter.createTeams(body);
        } catch (JsonSyntaxException e) {
            throw new IOException("Malformed data "+body);
        }
    }

    /**
     *
     * @param url the api url
     * @return
     */
    public static ArrayList<Player> getPlayers(String url) throws IOException {
        Request request = new Builder().url(url).build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String body = response.body().string();

        try {
            return JsonToModelConverter.createPlayers(body);
        } catch (JsonSyntaxException e) {
            throw new IOException("Malformed data "+body);
        }

    }

    /**
     * Automatically add commons parameters to the requests
     * Header,
     */
    private static class Builder extends Request.Builder{
        static final String HEADER_TOKEN_KEY = "X-Auth-Token";
        static final String HEADER_TOKEN_VALUE = "f7f2051260a8417b8eae9fb4de617af3";


        Builder() {
            addHeader(HEADER_TOKEN_KEY,HEADER_TOKEN_VALUE);
        }

    }
}
