package com.github.bubinimara.xceed;

import android.content.Context;

import com.github.bubinimara.xceed.database.DatabaseManager;
import com.github.bubinimara.xceed.http.HttpRequest;
import com.github.bubinimara.xceed.model.Player;
import com.github.bubinimara.xceed.model.Team;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.internal.Util;

/**
 * Created by davide on 14/10/16.
 *
 * Load data from database or from http if not hit
 *
 * Wrapper from pkgs Database and http
 *
 */

public class Loader {
    // Database Manager
    private DatabaseManager databaseManager;

    /**
     * It is safe to use multiple instance of this class
     * @param context the application context
     */
    public Loader(Context context) {
        databaseManager =  DatabaseManager.getInstance(context);
    }

    /**
     * Get all teams
     * @return Array of team
     * @throws IOException
     */
    public ArrayList<Team> getTeams() throws IOException {
        if(!databaseManager.validateTeam()){
            ArrayList<Team> teams = HttpRequest.getTeams();
            databaseManager.addTeams(teams);
            return teams;
        }

        return databaseManager.getTeams();
    }

    /**
     * Get all players
     * @param url used to retrieve the players
     * @return array of player
     * @throws IOException if an error occurs
     */
    public ArrayList<Player> getPlayer(String url) throws IOException {
        String keyHash = Util.md5Hex(url);
        if(!databaseManager.validatePlayer(keyHash)){
            ArrayList<Player> players = HttpRequest.getPlayers(url);
            databaseManager.addPlayers(keyHash,players);
            return players;
        }

        return databaseManager.getPlayers(keyHash);
    }
}
