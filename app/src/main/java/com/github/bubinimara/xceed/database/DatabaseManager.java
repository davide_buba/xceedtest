package com.github.bubinimara.xceed.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.github.bubinimara.xceed.model.Player;
import com.github.bubinimara.xceed.model.Team;

import java.util.ArrayList;

/**
 * Created by davide on 14/10/16.
 *
 * Is the only class that can be used outside this package
 */

public class DatabaseManager {

    private DatabaseHelper databaseHelper;
    private DatabaseInfo databaseInfo;

    private static DatabaseManager instance;

    private DatabaseManager(@NonNull Context context) {
        databaseHelper = DatabaseHelper.getInstance(context);
        databaseInfo = DatabaseInfo.getInstance(context);
    }

    public static DatabaseManager getInstance(@NonNull Context context) {
        if(instance == null){
            instance = new DatabaseManager(context);
        }

        return instance;
    }



    public synchronized void deleteAll(){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.delete(DatabaseContract.Player.TABLE_NAME,null,null);
        db.delete(DatabaseContract.Teams.TABLE_NAME,null,null);
    }

    public synchronized void addTeams(ArrayList<Team> teams){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.beginTransaction();

        for (Team t :
                teams) {
            db.insert(DatabaseContract.Teams.TABLE_NAME, null, ContentValueUtils.createTeamValues(t));
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

        databaseInfo.set("team",System.currentTimeMillis());
    }

    public synchronized ArrayList<Team> getTeams(){
        ArrayList<Team> teams = new ArrayList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String selection = null;
        String[] selectionArgs = null;
        Cursor c = db.query(true, DatabaseContract.Teams.TABLE_NAME,null,selection,selectionArgs,null,null,null,null);
        if(c!=null ){
            while(c.moveToNext()){
                teams.add(ContentValueUtils.createTeam(c));
            }
            c.close();
        }

        return teams;
    }

    public synchronized void deleteTeams(){
        databaseInfo.remove("team");
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        String where = null;
        String[] whereArgs = null;

        db.delete(DatabaseContract.Teams.TABLE_NAME, where, whereArgs);
        db.close();

    }

    /**
     * validate the teams stored in the database.
     * (Can be check with timestamp and set a time limits)
     *
     * @return true if the teams are stored in the database, false otherwise.
     *
     */
    public boolean validateTeam(){
        return databaseInfo.get("team")!=0;
    }

    public boolean validatePlayer(String keyHash){
        return databaseInfo.get(keyHash)!=0;
    }

    public synchronized void addPlayers(String keyHash, ArrayList<Player> players){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.beginTransaction();

        for (Player p :
                players) {
            db.insert(DatabaseContract.Player.TABLE_NAME, null, ContentValueUtils.createPlayerValues(keyHash,p));
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

        databaseInfo.set(keyHash,System.currentTimeMillis());
    }

    public synchronized ArrayList<Player> getPlayers(String keyHash){
        ArrayList<Player> players = new ArrayList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String selection = null;
        String[] selectionArgs = null;
        if(!TextUtils.isEmpty(keyHash)){
            selection = DatabaseContract.Player.COLUMN_HASH + "=?";
            selectionArgs = new String[]{keyHash};
        }
        Cursor c = db.query(true, DatabaseContract.Player.TABLE_NAME,null,selection,selectionArgs,null,null,null,null);
        if(c!=null ){
            while(c.moveToNext()){
                players.add(ContentValueUtils.createPlayer(c));
            }
            c.close();
        }

        return players;

    }

    public synchronized void deletePlayer(String keyHash){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        String where = null;
        String[] whereArgs = null;

        if(!TextUtils.isEmpty(keyHash)){
            where = DatabaseContract.Player.COLUMN_HASH + "=?";
            whereArgs = new String[]{keyHash};
        }

        db.delete(DatabaseContract.Player.TABLE_NAME, where, whereArgs);
        db.close();

        databaseInfo.remove(keyHash);
    }

    private static final class ContentValueUtils{
        static ContentValues createTeamValues(Team team){
            ContentValues cv = new ContentValues();
            cv.put(DatabaseContract.Teams.COLUMN_NAME,team.getName());
            cv.put(DatabaseContract.Teams.COLUMN_CODE,team.getCode());
            cv.put(DatabaseContract.Teams.COLUMN_SHORT_NAME,team.getShortName());
            cv.put(DatabaseContract.Teams.COLUMN_MARKET_VALUE,team.getSquadMarketValue());
            cv.put(DatabaseContract.Teams.COLUMN_CREST_URL,team.getCrestUrl());
            cv.put(DatabaseContract.Teams.COLUMN_PLAYER_URL,team.getPlayersUrl());
            return cv;
        }

        static Team createTeam(Cursor c){
            Team team = new Team();
            team.setName(c.getString(c.getColumnIndex(DatabaseContract.Teams.COLUMN_NAME)));
            team.setCode(c.getString(c.getColumnIndex(DatabaseContract.Teams.COLUMN_CODE)));
            team.setShortName(c.getString(c.getColumnIndex(DatabaseContract.Teams.COLUMN_SHORT_NAME)));
            team.setSquadMarketValue(c.getString(c.getColumnIndex(DatabaseContract.Teams.COLUMN_MARKET_VALUE)));
            team.setCrestUrl(c.getString(c.getColumnIndex(DatabaseContract.Teams.COLUMN_CREST_URL)));
            team.setPlayersUrl(c.getString(c.getColumnIndex(DatabaseContract.Teams.COLUMN_PLAYER_URL)));


            return team;
        }

        static ContentValues createPlayerValues(String keyHash,Player player) {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseContract.Player.COLUMN_HASH,keyHash);
            cv.put(DatabaseContract.Player.COLUMN_NAME,player.getName());
            cv.put(DatabaseContract.Player.COLUMN_POSITION,player.getPosition());
            cv.put(DatabaseContract.Player.COLUMN_NUMBER,player.getJerseyNumber());
            cv.put(DatabaseContract.Player.COLUMN_BIRTHDAY,player.getDateOfBirth());
            cv.put(DatabaseContract.Player.COLUMN_NATIONALITY,player.getNationality());
            cv.put(DatabaseContract.Player.COLUMN_CONTRACT_UNTIL,player.getContractUntil());
            cv.put(DatabaseContract.Player.COLUMN_MARKET_VALUE,player.getMarketValue());

            return cv;
        }

        public static Player createPlayer(Cursor c) {
            Player player = new Player();
            player.setName(c.getString(c.getColumnIndex(DatabaseContract.Player.COLUMN_NAME)));
            player.setPosition(c.getString(c.getColumnIndex(DatabaseContract.Player.COLUMN_POSITION)));
            player.setJerseyNumber(c.getInt(c.getColumnIndex(DatabaseContract.Player.COLUMN_NUMBER)));
            player.setDateOfBirth(c.getString(c.getColumnIndex(DatabaseContract.Player.COLUMN_BIRTHDAY)));
            player.setNationality(c.getString(c.getColumnIndex(DatabaseContract.Player.COLUMN_NATIONALITY)));
            player.setContractUntil(c.getString(c.getColumnIndex(DatabaseContract.Player.COLUMN_CONTRACT_UNTIL)));
            player.setMarketValue(c.getString(c.getColumnIndex(DatabaseContract.Player.COLUMN_MARKET_VALUE)));
            return player;
        }
    }
}
