package com.github.bubinimara.xceed.task;

/**
 * Created by davide on 14/10/16.
 */

public class AsyncTaskResult<T> {
    private T data;
    private Exception exception;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
