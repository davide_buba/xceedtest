package com.github.bubinimara.xceed.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by davide on 14/10/16.
 *
 * Store and load database info from shared preference
 */

class DatabaseInfo {
    // file name
    private static final String PREF_FILE = "databaseInfo";

    // Singleton class
    private static DatabaseInfo instance;

    private final SharedPreferences sharedPreferences;

    // all info
    private HashMap<String,Long> info = new HashMap<>();


    private DatabaseInfo(@NonNull Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_FILE,Context.MODE_PRIVATE);
        init();
    }

    /**
     * Get instance of this class
     * @param context the application context
     * @return single instance
     */
    static synchronized DatabaseInfo getInstance(@NonNull Context context) {
        if(instance==null){
            instance = new DatabaseInfo(context);
        }
        return instance;
    }

    /**
     * Load all values
     */
    private void init(){
        Map<String, ?> all = sharedPreferences.getAll();
        for (String key :
                all.keySet()) {
            info.put(key, (Long) all.get(key));
        }
    }

    public long get(String key){
        return info.get(key) == null ? 0 : info.get(key);
    }

    public void set(String key,long value){
        sharedPreferences.edit().putLong(key,value).apply();
        info.put(key,value);
    }

    public void remove(String key){
        sharedPreferences.edit().remove(key).apply();
        info.remove(key);
    }

    public void clear(){
        sharedPreferences.edit().clear().apply();
        info.clear();
    }

}
