package com.github.bubinimara.xceed.ui.svg;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.github.bubinimara.xceed.R;
import com.github.bubinimara.xceed.model.Player;
import com.github.bubinimara.xceed.task.AsyncTaskResult;
import com.github.bubinimara.xceed.task.PlayersAsyncTaskLoader;
import com.github.bubinimara.xceed.ui.adapter.PlayersAdapter;

import java.util.ArrayList;

public class PlayersFragment extends Fragment implements LoaderManager.LoaderCallbacks<AsyncTaskResult<ArrayList<Player>>>{
    private static final String ARG_POSITION = "arg_position";
    private static final String ARG_URL = "arg_url";

    private String url;
    private int position;

    private PlayersAdapter adapter;

    public PlayersFragment() {
        // Required empty public constructor
    }

    public static PlayersFragment newInstance(int position, String url) {
        PlayersFragment fragment = new PlayersFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        args.putString(ARG_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(ARG_POSITION);
            url = getArguments().getString(ARG_URL);
        }
        // else handle exception
        adapter = new PlayersAdapter(getContext());

        getLoaderManager().initLoader(100+position,null,this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_players, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        listView.setAdapter(adapter);
    }

    @Override
    public Loader<AsyncTaskResult<ArrayList<Player>>> onCreateLoader(int id, Bundle args) {
        return new PlayersAsyncTaskLoader(getContext(),url);
    }

    @Override
    public void onLoadFinished(Loader<AsyncTaskResult<ArrayList<Player>>> loader, AsyncTaskResult<ArrayList<Player>> data) {
        if(data.getException()==null){
            adapter.setPlayers(data.getData());
            adapter.notifyDataSetChanged();
        }else{
            // handle error
        }
    }

    @Override
    public void onLoaderReset(Loader<AsyncTaskResult<ArrayList<Player>>> loader) {

    }
}
