package com.github.bubinimara.xceed.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.github.bubinimara.xceed.Loader;
import com.github.bubinimara.xceed.model.Team;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by davide on 14/10/16.
 */
public class TeamsAsyncLoader extends AsyncTaskLoader<AsyncTaskResult<ArrayList<Team>>>{

    private AsyncTaskResult<ArrayList<Team>> result = new AsyncTaskResult<>();

    private Loader loader;

    private boolean isLoaded = false;

    public TeamsAsyncLoader(Context context) {
        super(context);
        loader = new Loader(context);
    }

    @Override
    public AsyncTaskResult<ArrayList<Team>> loadInBackground() {
            try {
                result.setData(loader.getTeams());
                result.setException(null);
            } catch (IOException e) {
                result.setException(e);
                result.setData(null);
            }
        return result;
    }


    @Override
    public void deliverResult(AsyncTaskResult<ArrayList<Team>> data) {
        this.result = data;
        if(isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(isLoaded){
            deliverResult(result);
        }else{
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();

    }

    @Override
    protected void onReset() {
        super.onReset();
        // unregister
    }
}
