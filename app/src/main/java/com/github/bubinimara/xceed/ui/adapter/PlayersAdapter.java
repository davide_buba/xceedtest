package com.github.bubinimara.xceed.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.bubinimara.xceed.R;
import com.github.bubinimara.xceed.model.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davide on 15/10/16.
 */

public class PlayersAdapter extends BaseAdapter {

    private ArrayList<Player> data = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public PlayersAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Player getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setPlayers(List<Player> players){
        data.clear();
        data.addAll(players);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.row_item_player,parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }
        holder.set(getItem(position));
        return convertView;
    }

    private static class Holder{
        private View view;
        private TextView textViewName;
        private TextView textViewPosition;
        private TextView textViewNationality;
        private TextView textViewNumber;

        public Holder(View view) {
            this.view = view;
            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewPosition = (TextView) view.findViewById(R.id.textViewPosition);
            textViewNationality = (TextView) view.findViewById(R.id.textViewNationality);
            textViewNumber = (TextView) view.findViewById(R.id.textViewNumber);
        }

        void set(Player player){
            textViewName.setText(player.getName());
            textViewPosition.setText(player.getPosition());
            textViewNationality.setText(player.getNationality());
            textViewNumber.setText(String.valueOf(player.getJerseyNumber()));
        }
    }
}
