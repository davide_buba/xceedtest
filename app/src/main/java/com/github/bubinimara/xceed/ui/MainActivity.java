package com.github.bubinimara.xceed.ui;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.github.bubinimara.xceed.R;
import com.github.bubinimara.xceed.model.Team;
import com.github.bubinimara.xceed.task.AsyncTaskResult;
import com.github.bubinimara.xceed.task.TeamsAsyncLoader;
import com.github.bubinimara.xceed.ui.adapter.TeamsAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<AsyncTaskResult<ArrayList<Team>>> {

    private static final String TAG = MainActivity.class.getSimpleName();
    private TeamsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        adapter = new TeamsAdapter(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.setAdapter(adapter);

        getSupportLoaderManager().initLoader(100,null,this);
    }

    @Override
    public Loader<AsyncTaskResult<ArrayList<Team>>> onCreateLoader(int id, Bundle args) {
        return new TeamsAsyncLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<AsyncTaskResult<ArrayList<Team>>> loader, AsyncTaskResult<ArrayList<Team>> data) {
        if(data.getException() != null){
            // handle error
            Log.i(TAG, "onLoadFinished: error "+data.getException().getMessage());
        }else{
            Log.i(TAG, "onLoadFinished: data "+data.getData().size());
            adapter.setData(data.getData());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(Loader<AsyncTaskResult<ArrayList<Team>>> loader) {

    }
}
