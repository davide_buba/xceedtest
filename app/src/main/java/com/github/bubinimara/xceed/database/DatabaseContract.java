package com.github.bubinimara.xceed.database;

import android.provider.BaseColumns;

/**
 * Contract class
 */
public final class DatabaseContract {
    private DatabaseContract() { }

    /**
     * Define columns and constants common to all tables
     */
    private interface ContractBaseColumns extends BaseColumns{

        // update time
        String COLUMN_UPDATE = "updateTime";

        // count column
        String COLUMN_COUNT = "count";


        String PROJECTION_COUNT[] = {
                " COUNT(*) AS count "
        };

    }

    /**
     * Define Team Contracts
     */
    public static final class Teams implements ContractBaseColumns {
        private Teams() {
        }

        public static final String TABLE_NAME = "Teams";

        public static final String COLUMN_NAME = "teamName";
        public static final String COLUMN_CODE = "code";
        public static final String COLUMN_SHORT_NAME = "shortName";
        public static final String COLUMN_MARKET_VALUE = "marketValue";
        public static final String COLUMN_CREST_URL = "crestUrl";
        public static final String COLUMN_PLAYER_URL = "playerUrl";
    }
    /**
     * Define Team Contracts
     */
    public static final class Player implements ContractBaseColumns {
        private Player() {
        }

        public static final String TABLE_NAME = "Players";

        // Workaround for id
        public static final String COLUMN_HASH = "hashId";

        public static final String COLUMN_NAME = "playerName";
        public static final String COLUMN_POSITION = "position";
        public static final String COLUMN_NUMBER = "jerseyNumber";
        public static final String COLUMN_BIRTHDAY = "dateOfBirth";
        public static final String COLUMN_NATIONALITY = "nationality";
        public static final String COLUMN_CONTRACT_UNTIL = "contractUntil";
        public static final String COLUMN_MARKET_VALUE = "marketValue";
    }
}
