package com.github.bubinimara.xceed.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.github.bubinimara.xceed.App;
import com.github.bubinimara.xceed.R;
import com.github.bubinimara.xceed.model.Team;
import com.github.bubinimara.xceed.ui.PlayersActivity;
import com.github.bubinimara.xceed.ui.svg.SvgDecoder;
import com.github.bubinimara.xceed.ui.svg.SvgDrawableTranscoder;
import com.github.bubinimara.xceed.ui.svg.SvgSoftwareLayerSetter;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by davide on 15/10/16.
 */

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.TeamHolder> implements AdapterListener {

    private LayoutInflater layoutInflater;
    private Context context;

    private ArrayList<Team> data = new ArrayList<>();



    public TeamsAdapter(Activity context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public TeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TeamHolder(layoutInflater.inflate(R.layout.row_item_team,parent,false),this);
    }

    @Override
    public void onBindViewHolder(TeamHolder holder, int position) {
        holder.set(getItem(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Team getItem(int position){
        return data.get(position);
    }

    public ArrayList<Team> getItems() {
        return data;
    }

    public void setData(ArrayList<Team> teams) {
        data.clear();
        data.addAll(teams);
    }

    @Override
    public void onItemClick(int position) {
        //Toast.makeText(context, "Item click "+position+" "+getItem(position).getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(context, PlayersActivity.class);
        intent.putParcelableArrayListExtra(PlayersActivity.EXTRA_ARG_TEAMS,data);
        intent.putExtra(PlayersActivity.EXTRA_ARG_POSITION,position);
        context.startActivity(intent);
    }


    static class TeamHolder extends RecyclerView.ViewHolder{
        private TextView textViewName;
        private ImageView imageView;


        TeamHolder(View itemView, final AdapterListener l) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    l.onItemClick(getAdapterPosition());
                }
            });
        }

        void set(Team team){

            textViewName.setText(team.getName());

            if(!TextUtils.isEmpty(team.getCrestUrl())){
                if(team.getCrestUrl().endsWith(".svg")){
                    App.getRequestBuilder()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            // SVG cannot be serialized so it's not worth to cache it
                            // and the getResources() should be fast enough when acquiring the InputStream
                            .load(Uri.parse(team.getCrestUrl()))
                            .into(imageView);
                }else {
                    Glide.with(imageView.getContext())
                            .load(team.getCrestUrl())
                            .into(imageView);
                }
            }else{
                // handle exception
            }

        }
    }
}
