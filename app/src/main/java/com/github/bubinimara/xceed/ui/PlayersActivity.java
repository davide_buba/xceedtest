package com.github.bubinimara.xceed.ui;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.bubinimara.xceed.R;
import com.github.bubinimara.xceed.model.Team;
import com.github.bubinimara.xceed.ui.svg.PlayersFragment;

import java.util.ArrayList;

public class PlayersActivity extends AppCompatActivity {

    public static final String EXTRA_ARG_TEAMS = "extra_args_teams";
    public static final String EXTRA_ARG_POSITION = "extra_args_position";

    private ArrayList<Team> teams;
    private int position;

    private ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        teams = getIntent().getParcelableArrayListExtra(EXTRA_ARG_TEAMS);
        position = getIntent().getIntExtra(EXTRA_ARG_POSITION,0);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new PlayerPagerAdapter(getSupportFragmentManager()));
        mPager.setCurrentItem(position);

    }

    private class PlayerPagerAdapter extends FragmentPagerAdapter{

        public PlayerPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlayersFragment.newInstance(position,teams.get(position).getPlayersUrl());
        }

        @Override
        public int getCount() {
            return teams==null ? 0 : teams.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return teams == null ? super.getPageTitle(position) : teams.get(position).getName();
        }
    }

}
